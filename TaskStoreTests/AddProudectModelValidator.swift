//
//  AddProudectModelValidator.swift
//  TaskStoreTests
//
//  Created by Apple on 9/25/21.
//

import XCTest
@testable import TaskStore
class AddProudectModelValidator: XCTestCase, UIViewProtocol {
    
    
    var presenterAddProudect: AddProudectPresenter?
    var errorValue: ErrorValues?

    override func setUpWithError() throws {
        presenterAddProudect = AddProudectPresenter(delegate: self)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAddProudectModelValidator_whenValidName_ShouldRetunFalse(){
        let data = Data.init()
        presenterAddProudect?.addProductvalidate(name: "", Dscription: "", price: "", image: data,onCompletion: { (success,error) in
            errorValue = error
            })
       XCTAssertEqual(errorValue, ErrorValues.name)
    }
    func testAddProudectModelValidator_whenValidDscription_ShouldRetunFalse(){
        let data = Data.init()
         presenterAddProudect?.addProductvalidate(name: "nike", Dscription: "", price: "", image: data,onCompletion: { (success,error) in
            errorValue = error
            })
       XCTAssertEqual(errorValue, ErrorValues.Dscription)
    }
    func testAddProudectModelValidator_whenValidprice_ShouldRetunFalse(){
        let data = Data.init()
         presenterAddProudect?.addProductvalidate(name: "nike", Dscription: "Nike Good", price: "", image: data,onCompletion: { (success,error) in
            errorValue = error
            })
        XCTAssertEqual(errorValue, ErrorValues.Price)
    }
     func testAddProudectSucess_ShouldRetunTrue() {
        let image = #imageLiteral(resourceName: "13157538_14718351_480")
        let dataImage =  image.jpegData(compressionQuality: 0.7)!
         presenterAddProudect?.addProductvalidate(name: "nike", Dscription: "Nike Good", price: "500", image: dataImage,onCompletion: { (success,error) in
            errorValue = error
            })
        XCTAssertEqual(errorValue, ErrorValues(rawValue: "new proudect"))
    }
    
    func registrationDidFailed(message: String) -> Bool {
        return false
    }
    func connectToPresnter() {
        
    }
    
    func disconnectToPresnter() {
        
    }
}
