//
//  Colors.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import Foundation
import UIKit
extension DesignSystem{
    enum Colors : String {
        case Primary = "primaryText"
        case Second = "secondText"
        case Titel = "titelText"
        case Background = "BackgroundView"
        var color:UIColor{
            switch self {
            case .Primary:
                return UIColor(named: self.rawValue)!
            case .Second:
                return UIColor(named: self.rawValue)!
            case .Titel:
                return UIColor(named: self.rawValue)!
            case .Background:
                return UIColor(named: self.rawValue)!
                
            }
        }
    }
    
}
