//
//  Font.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import Foundation
enum Font :String {
    case Bold = "Montserrat-Bold"
    case regular  = "Montserrat-Regular"
}
