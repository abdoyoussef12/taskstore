//
//  protocol.swift
//  TaskStore
//
//  Created by Apple on 9/25/21.
//

import Foundation


protocol ListViewProtocol: UIViewProtocol {
    func onItemsRetrieval(proudects: [Product])
}
protocol UIViewProtocol: class {
 func connectToPresnter()
 func disconnectToPresnter()
 }
protocol AddProductsViewPresenter: class {
    func addTapped(with title: String,discerption:String,price:Float,image:Data,id:String)
}
protocol EditProductsViewPresenter: class {
    func upadteSelected(for index: Int,title: String,discerption:String,price:Float,image:Data,id:String)
}
protocol Afterupdateproduct {
    func reloadData()
}
protocol UserFilter: class {
    func filter(name:String,price:Float,isfilter:Bool,nameEmpty:Bool)
}
protocol ValidationSearch: class {
    func validateName(name:String)
}
protocol DeletProductsViewPresenter: class {
    func deleteSelected(for index: Int,id:String)
}
