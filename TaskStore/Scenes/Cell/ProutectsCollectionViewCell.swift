//
//  ProutectsCollectionViewCell.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import UIKit

class ProutectsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageProudect: UIImageView!
    @IBOutlet var nameProudect: UILabel!
    @IBOutlet var priceProudect: UILabel!
    @IBOutlet var decripProudect: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func confige(proudect : Product){
        nameProudect.text = proudect.title
        priceProudect.text =  "$" + "" + proudect.price.description
        decripProudect.text = proudect.discerption
        imageProudect.image = UIImage(data:proudect.image!)
    }

}
