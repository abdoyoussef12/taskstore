//
//  AddProductPresnter.swift
//  TaskStore
//
//  Created by Apple on 9/25/21.
//

import Foundation
import RealmSwift



class AddProudectPresenter :AddProductsViewPresenter{
    var delegate: UIViewProtocol
    private  var realm = try! Realm()

    init(delegate: UIViewProtocol) {
        self.delegate = delegate
    }
    func addTapped(with title: String,discerption:String,price:Float,image:Data,id:String) {
        print("View notifies the Presenter that an add button was tapped.")
        addItem(title: title,discerption:discerption,price:price,image:image, id: id)
    }
    private func addItem(title: String,discerption:String,price:Float,image:Data,id:String) {
        print("Presenter adds an Item object to the Realm Database.")
        let item = Product(title: title, discerption: discerption, price: price , image: image, id: id)
        do {
            try self.realm.write {
                self.realm.add(item)
            }
        } catch {
            
        }
    }
    func addProductvalidate(name: String, Dscription: String, price: String, image:Data, onCompletion: ((Bool,ErrorValues?)->())) {
        if name.isEmpty{
            onCompletion(false,.name)
            return
        }
        if Dscription.isEmpty{
            onCompletion(false,.Dscription)
            return
        }
        if Dscription.count > 100 {
            onCompletion(false,.DscriptionCount)
            return
        }
        if price.isEmpty{
            onCompletion(false,.Price)
            return
        }
        if image.isEmpty{
            onCompletion(false,.image)
            return
        }
        onCompletion(true,ErrorValues(rawValue: "new proudect"))
        return

    }
    
    
}
enum ErrorValues: String {
    case name  = "name can't be blank"
    case Dscription  = "Dscription can't be blank"
    case DscriptionCount = "the max Dscription length is 100 char"
    case Price  = "full Price can't be blank"
    case image   =  "Please Choose Image"
    
}


