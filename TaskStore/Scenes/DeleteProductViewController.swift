//
//  DeletProudectViewController.swift
//  TaskStore
//
//  Created by Apple on 9/23/21.
//

import UIKit

class DeleteProductViewController: UIViewController {

    @IBOutlet var imageProudect: UIImageView!
    @IBOutlet var nameProudectLabel: UILabel!
    @IBOutlet var priceProudectLabel: UILabel!
    @IBOutlet var descProudectLabel: UILabel!
    var deletDelget : Afterupdateproduct!
    var EditDelget : EditProudectProtocol!
    var indexDelet  = 0
    var presenter: DeletePresenter!
    var proudects : [Product]?
    var id  = ""
    init(deletDelget:Afterupdateproduct,EditDelget:EditProudectProtocol) {
        self.deletDelget = deletDelget
        self.EditDelget =  EditDelget
        super.init(nibName: "\(DeleteProductViewController.self)", bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectToPresnter()
        SetupUI()
    }
    override func viewDidDisappear(_ animated: Bool) {
        disconnectToPresnter()
    }
    func SetupUI(){
        indexDelet = proudects?.firstIndex(where: {$0.id == id}) ?? 0
        nameProudectLabel.text = proudects?[indexDelet].title
        priceProudectLabel.text =   (proudects?[indexDelet].price.description)
        descProudectLabel.text = proudects?[indexDelet].discerption
        imageProudect.image = UIImage(data:(proudects?[indexDelet].image!)!)
       
        id = proudects?[indexDelet].id ?? ""
    }

    @IBAction func EditButton(_ sender: Any) {
        dismiss(animated: true) {
            self.EditDelget.EditProudect(index: self.indexDelet, id: self.id)
        }
       
    }
    
    @IBAction func DeleteButton(_ sender: Any) {
        presenter.deleteSelected(for: indexDelet, id: id)
        dismiss(animated: true) {
            self.deletDelget.reloadData()
        }
    }
}

extension DeleteProductViewController: UIViewProtocol {
    
    func connectToPresnter() {
        presenter = DeletePresenter(view: self, prouduct: proudects ?? [])
    }

    func disconnectToPresnter() {
        presenter = nil
    }
    
    
}
