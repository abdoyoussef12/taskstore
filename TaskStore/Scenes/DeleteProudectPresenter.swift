//
//  DeleteProudectPresenter.swift
//  TaskStore
//
//  Created by Apple on 9/25/21.
//
import Foundation
import RealmSwift

class DeletePresenter: DeletProductsViewPresenter {
       private var realm = try! Realm()
       var products : [Product]?
       let view : UIViewProtocol?
    required init(view: UIViewProtocol,prouduct: [Product]) {
        self.view = view
        self.products = prouduct
          }
    
    // MARK: - Protocol methods
    func viewDidLoad() {
      
    }
    func deleteSelected(for index: Int,id:String) {
        deleteItem(at: index, id: id)
    }
    private func deleteItem(at index: Int,id:String) {
        if let items = products {
            do {
                try self.realm.write {
                    if items.isEmpty == false{
                    let index =  items.firstIndex(where: {$0.id == id})
                    self.realm.delete(items[index!])
                        }
                }
            } catch {
                print("Couldn't delete an item")
            }
        }
    }
    
}

