//
//  EditeProudectViewController.swift
//  TaskStore
//
//  Created by Apple on 9/24/21.
//

import UIKit

class EditProductViewController:  UIViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate,Alertable{
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var nameTextFiled: UITextField!
    @IBOutlet private weak var discerptionTextFiled: UITextField!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet private weak var priceTextFiled: UITextField!
    var presenter: EditeProudectPresenter!
    private var deleget: Afterupdateproduct!
    var images: Data?
    var imagePicker = UIImagePickerController()
    var products :[Product] = []
    var indexDelete  = 0
    var id = ""
    init(deleget:Afterupdateproduct,EditDelget:EditProudectProtocol) {
        self.deleget = deleget
        super.init(nibName: "\(EditProductViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        connectToPresnter()
        SetupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        sucessAddProuduct()
    }
    func sucessAddProuduct(){
        let bounds = saveButton.bounds
        
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut , animations: {
            self.saveButton.frame.origin.x -= 60
            self.saveButton.frame.origin.y -= 60
            self.saveButton.bounds = CGRect(x: bounds.origin.x + 30  , y:  bounds.origin.y + 30 , width: bounds.size.width , height: bounds.size.height)
            
            
        }) { (completed) in
        }
    }
    func SetupUI(){
        indexDelete = products.firstIndex(where: {$0.id == id}) ?? 0
        nameTextFiled.text = products[indexDelete].title
        priceTextFiled.text =   (products[indexDelete].price.description)
        discerptionTextFiled.text = products[indexDelete].discerption
        imageView.image = UIImage(data:(products[indexDelete].image!))
        images = products[indexDelete].image
        id = products[indexDelete].id ?? ""
    }
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func imageSelectButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            images =  image.jpegData(compressionQuality: 0.7)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addProudctButton(_ sender: Any) {
      
        self.presenter?.editProudectValid(name: nameTextFiled.text!, Dscription: discerptionTextFiled.text!, price: priceTextFiled.text!, image: images!) {  [weak self] (success, error) in
            if (error != nil) {
                showAlertError(message: error?.rawValue ?? "")
            }else{
                presenter.upadteSelected(for: indexDelete, title: nameTextFiled.text!, discerption: discerptionTextFiled.text!,price: Float(priceTextFiled.text!)!, image: images!, id: id)
                dismiss(animated: true) {
                    self?.deleget.reloadData()
                }
        }
    }
    }
    
}
extension EditProductViewController: UIViewProtocol {
    func connectToPresnter() {
        presenter = EditeProudectPresenter.init(delegate: self, products: products)
    }
    
    func disconnectToPresnter() {
        presenter = nil
    }
    
    
    
}
