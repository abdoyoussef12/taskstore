//
//  EditeProudectPresenter.swift
//  TaskStore
//
//  Created by Apple on 9/25/21.
//

import Foundation
import RealmSwift
class EditeProudectPresenter :EditProductsViewPresenter{
    var delegate: UIViewProtocol
    private  var realm = try! Realm()
    var products : [Product]?
    
    init(delegate: UIViewProtocol,products:[Product]) {
        self.delegate = delegate
        self.products = products
    }
    func upadteSelected(for index: Int,title: String,discerption:String,price:Float,image:Data,id:String){
        editProduct(at: index, title: title,discerption:discerption,price:price,image:image, id: id)
    }
    private  func editProduct(at index: Int,title: String,discerption:String,price:Float,image:Data,id:String) {
        if let items = products?[index] {
            do {
                try self.realm.write {
                    items.discerption  =  discerption
                    items.title  =  title
                    items.price  =  price
                    items.image  =  image
                    
                }
            } catch {
                print("Couldn't delete an item")
            }
            
        }
    }
    func editProudectValid(name: String, Dscription: String, price: String, image:Data, onCompletion: ((Bool,ErrorValues?)->())) {
        if name.isEmpty{
            onCompletion(false,.name)
            return
        }
        if Dscription.isEmpty{
            onCompletion(false,.Dscription)
            return
        }
        if Dscription.count > 100 {
            onCompletion(false,.DscriptionCount)
            return
        }
        if price.isEmpty{
            onCompletion(false,.Price)
            return
        }
        if image.isEmpty{
            onCompletion(false,.image)
            return
        }
        onCompletion(true,ErrorValues(rawValue: "Edit"))
        return
        
    }
    
    
}
