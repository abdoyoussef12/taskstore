//
//  ItemsPresenter.swift
//  MVP-Realm
//
//  Created by Zafar on 1/28/20.
//  Copyright © 2020 Zafar. All rights reserved.
//

import Foundation
import RealmSwift

protocol ProductsViewPresenter: class {
    init(view: ListViewProtocol)
    func viewDidLoad()
    func filter(Price:Float,name:String, isfilter: Bool, nameEmpty: Bool)
}

class ItemsPresenter: ProductsViewPresenter {
    weak var view: ListViewProtocol?
    private var realmproducts: Results<Product>?
    private var realm = try! Realm()
    var products : [Product] = []
    var filterproducts : [Product] = []
    required init(view: ListViewProtocol) {
        self.view = view
    }
    func viewDidLoad() {
        retrieveItems()
    }
    func filter(Price:Float,name:String, isfilter: Bool, nameEmpty: Bool) {
        if nameEmpty == false{
            filterproducts = products.filter { $0.title == name && $0.price == Price }
        }else{
            filterproducts =  products.filter {  $0.price == Price }
        }
    }
    private func retrieveItems() {
        self.realmproducts = realm.objects(Product.self)
        
        let Products: [Product]? = self.realmproducts?
            .compactMap { $0 }
        view?.onItemsRetrieval(proudects: Products ?? [])
    }
}
