//
//  AddProudectViewController.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import UIKit
class AddProudectViewController: UIViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate,Alertable{
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var nameTextFiled: UITextField!
    @IBOutlet private weak var discerptionTextFiled: UITextField!
    @IBOutlet private weak var priceTextFiled: UITextField!
    var presenter: AddProudectPresenter!
    var reloadDelget : AddNewProudectsProtocol!
    var images: Data?
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        connectToPresnter()
    }
    override func viewDidDisappear(_ animated: Bool) {
        disconnectToPresnter()
    }
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true)
    }
    @IBAction func imageSelectButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            images =  image.jpegData(compressionQuality: 0.7)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func addProudctButton(_ sender: Any) {
        guard images != nil  else {
            showAlertError(message: "Please Choose Image")

            return
        }
        self.presenter?.addProductvalidate(name: nameTextFiled.text!, Dscription: discerptionTextFiled.text!, price: priceTextFiled.text!, image: images!) {  [weak self] (success, error) in
            if (error != nil) {
                showAlertError(message: error?.rawValue ?? "")
            }else{
                self?.presenter.addTapped(with: nameTextFiled.text!, discerption: discerptionTextFiled.text!, price: Float(priceTextFiled.text!) ?? 0, image:images!, id:  UUID().uuidString)
                dismiss(animated: true) {
                    self?.reloadDelget.addNewProudect()
                }
            }
        }
    }
    
}
extension AddProudectViewController:UIViewProtocol{
    func connectToPresnter() {
        presenter = AddProudectPresenter.init(delegate: self)
        
    }
    
    func disconnectToPresnter() {
        presenter = nil
    }
    
}
