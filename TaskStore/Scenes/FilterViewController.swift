//
//  FilterViewController.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import UIKit

class FilterViewController: UIViewController, ValidationSearch {
    @IBOutlet var nameFilterTextFiled: UITextField!
    @IBOutlet var lowPriceLabel: UILabel!
    @IBOutlet var highPriceLabel: UILabel!
    @IBOutlet var sliderPrice: DesignableSlider!
    private var delegetFilter: UserFilter!
    private var validateFilter: ValidationSearch!
    var minValue : Float = 0
    var maxValue : Float = 0
    var Value : Float = 0

    init(delegetFliter:UserFilter) {
        self.delegetFilter = delegetFliter
        super.init(nibName: "\(FilterViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        validateFilter.self = self
    }
    
    func setValue(product:[Product]){
            self.maxValue = product.map{$0.price}.max()!
            self.minValue = product.map{$0.price}.min()!
        self.Value = self.minValue
        DispatchQueue.main.async {
            self.lowPriceLabel.text = self.minValue.description
            self.highPriceLabel.text = self.maxValue.description

        }

    }
    @IBAction func sliderChange(_ sender: UISlider) {
        sliderPrice.setThumbImage(UIImage(named: "Asset 9"), for: UIControl.State.highlighted)
        sliderPrice.minimumValue = minValue
        sliderPrice.maximumValue  = maxValue
        Value = sliderPrice.value
        print("value\(sliderPrice.value)")
        
    }
    
    @IBAction func filterButton(_ sender: Any) {
        validateName(name: nameFilterTextFiled.text ?? "")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetButton(_ sender: Any) {
        delegetFilter.filter(name: "", price: 0.0, isfilter: false, nameEmpty: false)
        dismiss(animated: true, completion: nil)

        
    }
    func validateName(name: String) {
        if name != "" {
            delegetFilter.filter(name: nameFilterTextFiled.text ?? "", price: Value, isfilter: true, nameEmpty: false)
        }else{
            delegetFilter.filter(name: nameFilterTextFiled.text ?? "", price: Value, isfilter: true, nameEmpty: true)

        }
    }
}
