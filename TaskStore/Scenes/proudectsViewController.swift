//
//  proudectsViewController.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import UIKit
class proudectsViewController: UIViewController,UIViewProtocol,Afterupdateproduct, EditProudectProtocol ,Alertable{
    
    
    @IBOutlet private weak var lineIamge: UIImageView!
    @IBOutlet private weak var boxImage: UIImageView!
    @IBOutlet private weak var backgroudIamge: UIImageView!
    @IBOutlet private  weak var viewEmptyProudects: UIView!
    @IBOutlet  private weak var collectionView: UICollectionView!
    @IBOutlet var viewSucessAddProudect: UIView!
    var presenter: ItemsPresenter!
    var isSearch : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        Register()
        connectToPresnter()
        animationboxImage()
    }
    private  func Register(){
        collectionView.registerCell(cellClass: ProutectsCollectionViewCell.self)
    }
    func animationboxImage(){
        let bounds = boxImage.bounds
        UIView.animate(withDuration: 2, delay: 0.3, options: [.repeat, .autoreverse] , animations: {
            self.boxImage.frame.origin.y -= 10
            self.lineIamge.alpha = 1
            self.backgroudIamge.bounds = CGRect(x: bounds.origin.x  , y:  bounds.origin.y , width: bounds.size.width - 30, height: bounds.size.height)
        }) { (completed) in
        }
    }
    func sucessAddProuduct(){
        self.viewSucessAddProudect.isHidden = false
        UIView.animate(withDuration: 2, delay: 0.3, options: .curveEaseInOut , animations: {
            self.viewSucessAddProudect.frame.origin.y -= 60
            
            self.viewSucessAddProudect.alpha = 1
        }) { (completed) in
            self.viewSucessAddProudect.alpha = 0
        }
    }
    @IBAction func addProudectButton(_ sender: Any) {
        let view = AddProudectViewController()
        view.reloadDelget = self
        view.modalPresentationStyle = .fullScreen
        present(view, animated: true, completion: nil)
    }
    @IBAction func filterButton(_ sender: Any) {
        if presenter.products.count > 1 {
            let view = FilterViewController(delegetFliter: self)
            view.setValue(product: presenter.products)
            present(view, animated: true, completion: nil)
        } else{
            showAlertError(title: "", message: "You must have more than one product ")
        }
    }
    func connectToPresnter() {
        presenter.viewDidLoad()
    }
    
    func disconnectToPresnter() {}
    func reloadData() {
        DispatchQueue.main.async {
            self.presenter.viewDidLoad()
        }
    }
}

extension proudectsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearch == false{
            if presenter.products.count == 0{
                viewEmptyProudects.isHidden = false
                collectionView.isHidden = true

            }else{
                viewEmptyProudects.isHidden = true
                collectionView.isHidden = false
            }
            return  presenter.products.count
        }else{
            if presenter.filterproducts.count == 0{
                viewEmptyProudects.isHidden = false
                collectionView.isHidden = true

            }else{
                viewEmptyProudects.isHidden = true
                collectionView.isHidden = false
            }
            return presenter.filterproducts.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isSearch == false{
            let cell = collectionView.dequeue(indexPath: indexPath) as ProutectsCollectionViewCell
            cell.confige(proudect: presenter.products[indexPath.row])
            return cell
        }else{
            let cell = collectionView.dequeue(indexPath: indexPath) as ProutectsCollectionViewCell
            cell.confige(proudect: presenter.filterproducts[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.bounds.width / 2.2, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let view = DeleteProductViewController(deletDelget: self, EditDelget: self)
        view.proudects =  presenter.products
        view.id =  presenter.products[indexPath.row].id ?? ""
        view.indexDelet = indexPath.row
        present(view, animated: true, completion: nil)
    }
}
extension proudectsViewController: ListViewProtocol , UserFilter,AddNewProudectsProtocol{
    func onItemsRetrieval(proudects: [Product]) {
        if proudects.isEmpty {
            viewEmptyProudects.isHidden = false
        }else{
            viewEmptyProudects.isHidden = true
            presenter.products = proudects
            DispatchQueue.main.async {
                self.collectionView.reloadData()

            }
            
        }
    }
    func addNewProudect() {
        presenter.viewDidLoad()
        sucessAddProuduct()
    }
    func filter(name: String, price: Float, isfilter: Bool, nameEmpty: Bool) {
       
            if nameEmpty == false{
                presenter.filter(Price: price, name: name, isfilter: isfilter, nameEmpty: nameEmpty)
                isSearch = isfilter
                DispatchQueue.main.async {
                    self.collectionView.reloadData()

                }            }else{
                presenter.filter(Price: price, name: name, isfilter: isfilter, nameEmpty: nameEmpty)
                isSearch = isfilter
                DispatchQueue.main.async {
                    self.collectionView.reloadData()

                }
            }
       
    }
    func EditProudect(index :Int, id: String) {
        let view = EditProductViewController(deleget: self, EditDelget: self)
        view.id = id
        view.products = self.presenter.products
        view.modalPresentationStyle = .overFullScreen
        present(view, animated: true, completion: nil)
    }
    
}
protocol EditProudectProtocol {
    func EditProudect(index :Int , id: String)
}
protocol AddNewProudectsProtocol {
    func addNewProudect()
}

