
import RealmSwift

class Product: Object {
    
    @objc dynamic var title: String = ""
    @objc dynamic var discerption: String = ""
    @objc dynamic var price: Float  = 0
    @objc dynamic var image : Data? = nil
    @objc dynamic var id : String? = ""
    override class func primaryKey() -> String? {
           return "id"
       }
    convenience init(title: String,discerption:String,price:Float,image:Data,id:String) {
        self.init()
        self.title = title
        self.discerption = discerption
        self.price = price
        self.image = image
        self.id = id
    }
}
