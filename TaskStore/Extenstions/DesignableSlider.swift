//
//  DesignableSlider.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import UIKit
@IBDesignable
class DesignableSlider: UISlider {
    @IBInspectable var thumbImage : UIImage?{
        didSet{
            setThumbImage(thumbImage, for: .normal)
        }
    }
}
