//
//  ListRouterInput.swift
//  TaskStore
//
//  Created by Apple on 9/22/21.
//

import Foundation
import UIKit

struct ListRouterInput {

    private func view(controller:UIViewController) -> UIViewController {
        let view = controller
        return view
    }

    func push(from: Viewable,controller:UIViewController) {
        let view = self.view(controller: controller)
        from.push(view, animated: true)
    }

    func present(from: Viewable,controller:UIViewController) {
        let nav = UINavigationController(rootViewController: view(controller: controller))
        nav.modalPresentationStyle = .overFullScreen
        from.present(nav, animated: true)
    }
    func presentNotFullScreen(from: Viewable,controller:UIViewController) {
        let view = self.view(controller: controller)
        from.present(view, animated: true)
    }
}

final class ListRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }

   
}
